(function(){
    'use strict';
    var module = angular.module('app', ['onsen']);

    module.controller('AppController', function($scope, $data, databaseService) {
        document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );
        //databaseService.createDB();
        function onDeviceReady() {

            console.log('onDeviceReady');
            
            document.addEventListener( 'pause', onPause.bind( this ), false );
            document.addEventListener( 'resume', onResume.bind( this ), false );
            databaseService.createDB();
            
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
                $data.rootPath = fileSystem.root.toURL();
                console.log("file root path: " + $data.rootPath);
            }, fail);
            
            function fail(error) {
                alert("Can't get root path: " + JSON.stringify(error));
            }
        };

        function onPause() {
            // TODO: This application has been suspended. Save application state here.
        };

        function onResume() {
            // TODO: This application has been reactivated. Restore application state here.
        };
    });
    
    module.controller('DetailController', function($scope, $data) {
        $scope.item = $data.selectedItem;
    });
    
    module.controller('MasterController', function($scope, $data, apiService, databaseService, networkService) {
        $scope.showEPI = function() {
            $scope.navi.pushPage('epis.html');
        };
        $scope.showEIFU = function() {
            $scope.navi.pushPage('eifus.html');
        };
        $scope.showPreferenceCards = function() {
            $scope.navi.pushPage('pcs.html');
        };
        $scope.synchronize = function() {
            if ($data.platform == 'browser') {
                databaseService.clearDB();
                apiService.getAllData();
            }
            if ($data.platform == 'device') {
                if (networkService.isNetworkAvailable() == true) {
                    ActivityIndicator.show('Please wait');
                    databaseService.clearDB();
                    apiService.getAllData();
                } else {
                    alert('Network is not available.\nPlease check your network status');
                }    
            }            
        };
    });

    module.factory('$data', function() {
        var data = {};

        data.items = [
            {
              title: 'Item 1 Title',
              label: '4h',
              desc: 'Test Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
              tags: 'endo-mechanichal biosurgery',
              width: 'width2'
            },
            {
              title: 'Another Item Title',
              label: '6h',
              desc: 'Ut enim ad minim veniam.',
              tags: 'advanced-energy biosurgery',
              width: ''
            },
            {
              title: 'Yet Another Item Title',
              label: '1day ago',
              desc: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
              tags: 'endo-mechanichal',
              width: ''
            },
            {
              title: 'Yet Another Item Title',
              label: '1day ago',
              desc: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
              tags: 'wound-closure advanced-energy',
              width: 'width2'
            }
        ];

//        data.platform = 'device';        
        data.platform = 'browser';
        data.hospitalId = 1;
        
        data.serverAddress = 'http://192.168.1.130/ethicon';
        data.apiAddress = 'http://192.168.1.130/ethicon/index.php/api';
        
//        data.serverAddress = 'http://198.74.54.24/~jnj';
//        data.apiAddress = 'http://198.74.54.24/~jnj/index.php/api';
        
        data.categories = [
            {
                value: 1,
                checked: true,
                name: 'Endo Mechanical'
            },
            {
                value: 2,
                checked: false,
                name: 'Advanced Energy'
            },
            {
                value: 3,
                checked: false,
                name: 'Wound Closure'
            },
            {
                value: 4,
                checked: false,
                name: 'Biosurgery'
            }
        ];

        return data;
    });
  
    module.directive('repeatDirective', function($timeout) {
        return function($scope, element, attrs) {
            if ($scope.$last){
                $timeout(function () {
                    $scope.$emit('LastElem');
                });
            }
        };
    });
})();