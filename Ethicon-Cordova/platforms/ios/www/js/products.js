(function() {

    var module = angular.module('app');

    module.controller('ProductsController', function($scope, $data) {
        
        $scope.arrProducts = [];
        $scope.categoryName = $data.categories[$data.category_id - 1].name;
        $scope.serverAddress = $data.serverAddress;

        for (var i = 0; i < $data.arrProducts.length; i++) {
            if ($data.category_id == $data.arrProducts[i].category_id) {
                $scope.arrProducts.push($data.arrProducts[i]);
            }
        }

        $scope.go_detail = function(product) {
            $data.product = product;
            //$scope.navi.pushPage('product_detail.html');
//            ons.ready(function() {
//                ons.createDialog('product_detail_dialog.html').then(function(dialog) {
//                    dialog.show();
//                });
//            });

            ons.createDialog('product_detail_dialog.html').then(function(dialog) {
                dialog.show();
            });
        }

        $scope.go_add = function() {
            $scope.navi.pushPage('product_add.html');
        }
    });
})();