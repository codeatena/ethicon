(function(){
    
    var module = angular.module('app');
    
    module.controller('SurgeonDetailController', function($scope, $data) {
        var surgeon = $data.surgeon;
        $scope.serverAddress = $data.serverAddress;
        
        var arrPCs = [];
        for (var i = 0; i < $data.arrPCs.length; i++) {
            var pc = $data.arrPCs[i];
            if (pc.surgeon_id == surgeon.id) {
                arrPCs.push(pc);
            }
        }
        
        surgeon.arrPCs = arrPCs;
        $scope.surgeon = surgeon;
        $scope.arrSurgeonPCs = arrPCs;

        $scope.go_detail = function(pc) {
            $data.pc = pc;
            $scope.navi.pushPage('pc_detail.html');
        };
    });
})();