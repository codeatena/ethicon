(function() {
    var module = angular.module('app');
    
    module.controller('LinksController', function($scope, $data) {

        $scope.arrLinks = $data.arrLinks;
        $scope.serverAddress = $data.serverAddress;
                
        $scope.go_detail = function(link) {
            $data.link = link;
            $scope.navi.pushPage('link_detail.html');
        }

        $scope.go_add = function() {
            $scope.navi.pushPage('link_add.html');
        }
    });
})();