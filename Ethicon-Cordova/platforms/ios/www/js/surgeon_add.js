(function(){
    var module = angular.module('app');
    
    module.controller('SurgeonAddController', function($scope, $http, $data, apiService, networkService, databaseService) {
        $scope.surgeon_add_done = function() {
            var url = $data.apiAddress + '/createsurgeon/';
            console.log('create surgeon url: ' + url);
            
            var postData = {};
            postData.name = $scope.addedSurgeonName;
            postData.specialty = $scope.addedSurgeonSpecialty;
            postData.contact_number = $scope.addedSurgeonContactNumber;
            postData.hospital_id = $data.hospitalId;
            
            if (networkService.isNetworkAvailable() == true) {
                ActivityIndicator.show('Please wait');
                
                $http.post(url, postData,
                {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    transformRequest: apiService.transform
                }).success(function(data, status, headers, config) {
                    alert('New Surgeon is created successfully.');
                    console.log("create surgeon response: " + JSON.stringify(data));
                    databaseService.clearDB();
                    apiService.getAllData();
                    $scope.navi.popPage();
                })
                .error(function(data, status, headers, config) {
                    alert("Error: " + JSON.stringify(data));
                });
            } else {
                alert('Network is not available.\nPlease check your network status');
            }
        }
    });
})();