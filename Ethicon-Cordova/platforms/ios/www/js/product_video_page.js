(function(){
    'use strict';
    var module = angular.module('app');
    
    module.controller('ProductVideoPageController', function($scope, $data) {
        $scope.product = $data.product;
        $scope.serverAddress = $data.serverAddress;
        
        document.getElementById("productVideoSource").src = $data.serverAddress + "/" + $scope.product.video_url;
    });
})();