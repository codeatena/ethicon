angular.module('app').factory('fileService',
    function($data) {
        var factory = {};
        
        factory.updateImage = function(fileURI) {
            ActivityIndicator.show('Please wait');
            function clearCache() {
                navigator.camera.cleanup();
            }
           
            var retries = 0;
            var win = function(r) {
                clearCache();
                retries = 0;
                ActivityIndicator.hide();
                alert('upload Done!');
            }
            
            var fail = function(error) {
                ActivityIndicator.hide();
                if (retries == 0) {
                    retries++;
                    setTimeout(function() {
                        fileService.updateImage(fileURI)
                    }, 1000);
                } else {
                    retries = 0;
                    clearCache();
                    alert('Ups. Something wrong happens!' + JSON.stringify(error));
                }
            }
            
            var options = new FileUploadOptions();
            options.fileKey = "file";
            options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
            options.mimeType = "image/jpeg";
            options.params = { id: "thisistheid", hospital_id:$data.hospitalId }; // if we need to send parameters to the server request
            var ft = new FileTransfer();
            var url = $data.serverAddress +'/createpreferencecard';
            ft.upload(fileURI, encodeURI(url), win, fail, options);
        };
        
        factory.downloadImage = function(imageUrl, fileName) {
            
            if ($data.platform == 'device') {
                console.log('file download image url: ' + imageUrl);
            
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
                    fileSystem.root.getFile(fileName, {create: true, exclusive: false},
                        function(fileEntry) {
                            var localPath = fileEntry.fullPath;
    //                        if (device.platform === "Android" && localPath.indexOf("file://") === 0) {
    //                            localPath = localPath.substring(7);
    //                        }
                            var ft = new FileTransfer();
                            ft.download(encodeURI(imageUrl), fileEntry.toURL(), 
                                function(entry) {                         
                                    console.log('file donwload done: ' + fileEntry.toURL());
                                }, fail);
                        }, fail);
                }, fail);
                
                function fail(error) {
                    alert("file download error: " + JSON.stringify(error));
                }
            }

        };

        return factory;
    }
);