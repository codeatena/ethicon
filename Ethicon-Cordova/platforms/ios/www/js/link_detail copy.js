(function(){
    'use strict';
    var module = angular.module('app');
    
    module.controller('LinkDetailController', function($scope, $data) {
        $scope.link = $data.link;
    });
})();