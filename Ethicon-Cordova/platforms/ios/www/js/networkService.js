angular.module('app').factory('networkService',
    function() {
        var factory = {};
        
        factory.isNetworkAvailable = function() {
            var networkState = navigator.connection.type;

            var states = {};
            states[Connection.UNKNOWN]  = 'Unknown connection';
            states[Connection.ETHERNET] = 'Ethernet connection';
            states[Connection.WIFI]     = 'WiFi connection';
            states[Connection.CELL_2G]  = 'Cell 2G connection';
            states[Connection.CELL_3G]  = 'Cell 3G connection';
            states[Connection.CELL_4G]  = 'Cell 4G connection';
            states[Connection.CELL]     = 'Cell generic connection';
            states[Connection.NONE]     = 'No network connection';

            console.log('Connection type: ' + states[networkState]);
            //alert('Connection type: ' + states[networkState]);
            if (networkState == Connection.WIFI) {
                return true;
            }
            if (networkState == Connection.CELL) {
                return true;
            }
            if (networkState == Connection.CELL_2G) {
                return true;
            }
            if (networkState == Connection.CELL_3G) {
                return true;
            }
            if (networkState == Connection.CELL_4G) {
                return true;
            }
            
            return false;
        };

        return factory;
    }
);