angular.module('app').factory('databaseService',
    function($data, fileService, linkService) {
        var factory = {};
        
        factory.readDB = function() {
            linkService.readAllRecords();
        }
        
        factory.createDB = function() {
            console.log('createDB');
            var db = window.openDatabase("ethicon", "1.0", "Ethicon", 200000);
            db.transaction(populateDB, errorCB, successCB);
            
            function populateDB(tx) {
                console.log('createDB populateDB');

                tx.executeSql('CREATE TABLE IF NOT EXISTS surgeon (id, name, specialty, contact_number, hospital_id)');
                tx.executeSql('CREATE TABLE IF NOT EXISTS preference_card (id, card_url, surgeon_id, file_name)');
                tx.executeSql('CREATE TABLE IF NOT EXISTS product (id, name, pdf_url, video_url, image_url, category_id)');
                tx.executeSql('CREATE TABLE IF NOT EXISTS link (id, name, url, image_url)');
            }

            function errorCB(err) {
                console.log("Error create database: " + err.code);
            }

            function successCB() {
                console.log("database created succesfully.");
                factory.readDB();
            }
        };
        
        factory.clearDB = function() {
            console.log('clearDB');
            var db = window.openDatabase("ethicon", "1.0", "Ethicon", 200000);
            db.transaction(populateDB, errorCB, successCB);
            
            function populateDB(tx) {
                console.log('clearDB populateDB');
                tx.executeSql('DELETE FROM surgeon');
                tx.executeSql('DELETE FROM preference_card');
                tx.executeSql('DELETE FROM product');
                tx.executeSql('DELETE FROM link');
            }

            function errorCB(err) {
                console.log("Error create database: " + err.code);
            }

            function successCB() {
                console.log("database clean succesfully.");
            }
        };
        
        factory.addDatas = function(json) {
            var data = json.data;

            var products = data.products;
            var links = data.links;
            var surgeons = data.hospital.surgeons;

            var db = window.openDatabase("ethicon", "1.0", "Ethicon", 200000);
            db.transaction(addAllRecord, errorCB);
            
            function addAllRecord(tx) {
                console.log("addAllRecord");
                for (i = 0; i < links.length; i++) {
                    var link = links[i];
                    var sql = "INSERT INTO link (id, name, url, image_url) VALUES ('" + link.id 
                        + "', '" + link.name
                        + "', '" + link.url
                        + "', '" + link.image_url + "')";
                    console.log("link insert sql: " + sql);
                    tx.executeSql(sql);
                }
                for (i = 0; i < products.length; i++) {
                    var product = products[i];
                    var sql = "INSERT INTO product (id, name, pdf_url, video_url, image_url, category_id) VALUES ('" + product.id 
                        + "', '" + product.name
                        + "', '" + product.pdf_url
                        + "', '" + product.video_url
                        + "', '" + product.image_url
                        + "', '" + product.category_id + "')";
                    console.log("product insert sql: " + sql);
                    tx.executeSql(sql);
                }
                for (i = 0; i < surgeons.length; i++) {
                    var surgeon_preference_cards = surgeons[i];
                    var surgeon = surgeon_preference_cards[0];
                    var preference_cards = surgeon_preference_cards[1].preference_cards;
                    var sql = "INSERT INTO surgeon (id, name, specialty, contact_number, hospital_id) VALUES ('" + surgeon.id 
                        + "', '" + surgeon.name
                        + "', '" + surgeon.specialty
                        + "', '" + surgeon.contact_number  
                        + "', '" + surgeon.hospital_id + "')";
                    console.log("surgeon insert sql: " + sql);
                    tx.executeSql(sql);
                    
                    for (j = 0; j < preference_cards.length; j++) {
                        var preference_card = preference_cards[j];
                        var n = preference_card.card_url.lastIndexOf('.');
                        var fileExt = preference_card.card_url.substr(n);
                        var fileName = preference_card.card_url;
                        //alert('file name: ' + fileName);
                        var sql = "INSERT INTO preference_card (id, card_url, surgeon_id, file_name) VALUES ('" + preference_card.id 
                            + "', '" + preference_card.card_url 
                            + "', '" + preference_card.surgeon_id
                            + "', '" + fileName + "')";
                        console.log("preference_card insert sql: " + sql);
                        
                        tx.executeSql(sql);
                        var url = $data.serverAddress + '/img/uploads/' + fileName;
                        fileService.downloadImage(url, fileName);
                    }
                }
                
                factory.readDB();
                if ($data.platform == 'device') {
                    ActivityIndicator.hide();    
                }
                alert("Synchronized done sucessfully.");
            }
            
            function errorCB(err) {
                alert("Error processing SQL: " + JSON.stringify(err));
            }
        };

        return factory;
    }
);