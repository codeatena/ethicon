(function(){
    
    var module = angular.module('app');
    
    module.controller('SurgeonsController', function($scope, $data) {
        
        $scope.arrSurgeons = $data.arrSurgeons;
        
        $scope.go_detail = function(index) {
            $data.surgeon = $data.arrSurgeons[index];
            $scope.navi.pushPage('surgeon_detail.html');
        }
        
        $scope.go_add = function() {
            $scope.navi.pushPage('surgeon_add.html');
        }
    });
})();