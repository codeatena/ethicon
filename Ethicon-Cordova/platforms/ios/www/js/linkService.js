angular.module('app').factory('linkService',
    function($data, productService) {
        var factory = {};
        var tableName = 'link';
        
        factory.readAllRecords = function() {
            var arrRecords = [];
            console.log(tableName + ' table getRecords');
            var db = window.openDatabase("ethicon", "1.0", "Ethicon", 200000);
            db.transaction(populateDB, errorCB, successCB);
            
            function successCB() {
                console.log(tableName + " table read success.");
                $data.arrLinks = arrRecords;
                productService.readAllRecords();
            }
            
            function errorCB(err) {
                console.log(tableName + " Error processing SQL: "+err.code);
                alert("Error processing SQL: "+err.code);
            }
            
            function populateDB(tx) {
                console.log(tableName + " populateDB");
                tx.executeSql('SELECT * FROM ' +  tableName, [], querySuccess, errorCB);
            }
            
            function querySuccess(tx, results) {
                var len = results.rows.length;
                console.log(tableName + " table: " + len + " rows found.");
                for (var i = 0; i < len; i++) {
                    arrRecords[i] = results.rows.item(i);
                }
            }
        }
        
        return factory;
    }
);