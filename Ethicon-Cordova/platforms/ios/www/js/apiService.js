angular.module('app').factory('apiService',
    function($http, $data, databaseService) {
        'use strict';
        var factory = {};
                
        factory.getSurgeons = function(from, limit) {
            var url = $data.apiAddress + '/getsurgeons';
            $http.post(url, {
                    from: from,
                    limit: 20,
                    hospital_id: $data.hospitalId
                })
                .success(function(data, status, headers, config) {
                    alert(JSON.stringify(data));
                })
                .error(function(data, status, headers, config) {
                    alert("Error: " + JSON.stringify(data));
                });
        };
        
        factory.getAllData = function() {
            
            var url = $data.apiAddress + '/getalldata?hospital_id=' + $data.hospitalId;
            console.log('synchronize url: ' + url);
            $http.get(url)
                .success(function(data, status, headers, config) {
                    console.log("synchronize response: " + JSON.stringify(data));
                    databaseService.addDatas(data);
                })
                .error(function(data, status, headers, config) {
                    alert("Error: " + JSON.stringify(data));
                });
        };
        
        factory.transform = function(obj) {
            var str = [];
            for (var key in obj) {
                if (obj[key] instanceof Array) {
                    for(var idx in obj[key]){
                        var subObj = obj[key][idx];
                        for(var subKey in subObj){
                            str.push(encodeURIComponent(key) + "[" + idx + "][" + encodeURIComponent(subKey) + "]=" + encodeURIComponent(subObj[subKey]));
                        }
                    }
                } else {
                    str.push(encodeURIComponent(key) + "=" + encodeURIComponent(obj[key]));
                }
            }
            return str.join("&");
        };
        
        return factory;
    }
);