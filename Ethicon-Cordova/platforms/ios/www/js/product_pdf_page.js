(function(){
    'use strict';
    var module = angular.module('app');
    
    module.controller('ProductPdfPageController', function($scope, $data) {
        $scope.product = $data.product;
        
        document.getElementById("productPDFByUrl").src = $data.serverAddress + "/" + $scope.product.pdf_url;
    });
})();