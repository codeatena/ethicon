(function(){
    var module = angular.module('app');
    
    module.controller('ProductAddController', function($scope, $http, $data, networkService, databaseService, apiService) {
        
        $scope.arrCategories = $data.categories;
                
        $scope.product_add_done = function() {
            var url = $data.apiAddress + '/createproduct/';
            console.log('create product url: ' + url);
            
            var postData = {};
            postData.name = $scope.addedProductName;
            postData.pdf_url = $scope.addedProductPdfUrl;
            postData.video_url = $scope.addedProductVideoUrl;
            postData.category_id = $scope.curCategoryValue;
            
            if (postData.name == null) {
                alert("Please input name.");
                return;
            }
            if (postData.pdf_url == null) {
                alert("Please input PDF Url.");
                return;
            }
            if (postData.video_url == null) {
                alert("Please input Video Url.");
                return;
            }
            if ($scope.curCategoryValue == null) {
                alert("Please choose category.");
                return;
            }
            
            if (networkService.isNetworkAvailable() == true) {
                ActivityIndicator.show('Please wait');
                
                $http.post(url, postData,
                {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    transformRequest: apiService.transform
                }).success(function(data, status, headers, config) {
                    alert('New Product is created successfully.');
                    console.log("create product response: " + JSON.stringify(data));
                    databaseService.clearDB();
                    apiService.getAllData();
                    $scope.navi.popPage();
                }).error(function(data, status, headers, config) {
                    alert("Error: " + JSON.stringify(data));
                    ActivityIndicator.hide();
                });
            } else {
                alert('Network is not available.\nPlease check your network status');
            }
        }
    });
})();