(function(){
    var module = angular.module('app');
    
    module.controller('PCAddController', function($scope, $data, networkService, databaseService, apiService) {
        
        $scope.arrSurgeons = $data.arrSurgeons;
        $scope.image = null;

        $scope.pc_add_done = function() {
            
            if ($scope.selectedSurgeonID == null) {
                alert("Please select suregeon");
                return;
            }
            if ($scope.image == null) {
                alert("Please take picture");
                return;
            }
            if (networkService.isNetworkAvailable() == false) {
                alert('Network is not available.\nPlease check your network status');
                return;
            }
            
            ActivityIndicator.show('Please wait');

            function clearCache() {
                navigator.camera.cleanup();
            }
           
            var retries = 0;
            
            var win = function(r) {
                clearCache();
                retries = 0;
                ActivityIndicator.hide();
                alert('New Preference Card is created successfully.');
                databaseService.clearDB();
                apiService.getAllData();
                $scope.navi.popPage();
            }
            
            var fail = function(error) {
                alert("Error: " + JSON.stringify(data));
                ActivityIndicator.hide();
                
                /*
                if (retries == 0) {
                    retries++;
                    setTimeout(function() {
                        fileService.updateImage(fileURI);
                    }, 1000);
                } else {
                    retries = 0;
                    clearCache();
                    alert('Ups. Something wrong happens!'+JSON.stringify(error));
                }
                */
            }
            
            var fileURI = $scope.image;
            
            var options = new FileUploadOptions();
            options.fileKey = "file";
            options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
            options.mimeType = "image/jpeg";
            options.params = { surgeon_id: $scope.selectedSurgeonID }; // if we need to send parameters to the server request
            
            var ft = new FileTransfer();
            var url = $data.apiAddress +'/createpreferencecard/';
            
            ft.upload(fileURI, encodeURI(url), win, fail, options);
        }
        
        $scope.takePhoto = function() {
            navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
                destinationType: Camera.DestinationType.FILE_URI,
                correctOrientation: true
            });

            function onSuccess(fileUri) {
                var imageToShow = document.getElementById('preference_card_image');
                imageToShow.src = fileUri;
                $scope.image = fileUri;
            }

            function onFail(message) {
                alert('Failed because: ' + message);
            }
        };
      
        $scope.uploadPhoto = function(){
            alert('uploadPhoto');

//            fileService.updateImage($scope.image);
        };
    });
})();