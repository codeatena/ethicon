(function(){
    'use strict';
    var module = angular.module('app');
    
    module.controller('ProductDetailController', function($scope, $data) {
        $scope.product = $data.product;
    });
})();