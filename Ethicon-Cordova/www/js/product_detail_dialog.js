(function(){
    'use strict';
    var module = angular.module('app');
    
    module.controller('ProductDetailDialogController', function($scope, $data) {
        $scope.product = $data.product;
        
        $scope.product_pdf_view = function() {
//            alert('product_pdf_view');
            $scope.navi.pushPage('product_pdf_page.html');
            $scope.dlgProductDetail.hide();
        }
        
        $scope.product_video_view = function() {
            $scope.navi.pushPage('product_video_page.html');
            $scope.dlgProductDetail.hide();
        }
    });
})();