(function(){
    'use strict';
    var module = angular.module('app');
    
    module.controller('EpisController', function($scope, $data) {
        $scope.go_product = function(category_id) {
            $data.category_id = category_id;
            $scope.navi.pushPage('products.html');
        };
    });
})();