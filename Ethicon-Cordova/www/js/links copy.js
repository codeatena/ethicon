(function() {
    var module = angular.module('app');
    
    module.controller('LinksController', function($scope, $data) {

        $scope.arrLinks = $data.arrLinks;

        $scope.go_detail = function(index) {
            $data.link = $data.arrLinks[index];
            $scope.navi.pushPage('link_detail.html');
        }

        $scope.go_add = function() {
            $scope.navi.pushPage('link_add.html');
        }
    });
})();