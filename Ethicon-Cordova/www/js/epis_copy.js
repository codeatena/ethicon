(function(){
    'use strict';
    var module = angular.module('app');
    
    module.controller('EpisController', function($scope, $data) {
        $scope.items = $data.items;
        $scope.iso = null;

        $scope.showDetail = function(index) {
            var selectedItem = $data.items[index];
            $data.selectedItem = selectedItem;
            $scope.navi.pushPage('detail.html', { title: selectedItem.title });
        };

        $scope.filterBy = function(filter){
            $scope.iso.arrange({filter: filter});
        };

        $scope.$on('LastElem', function(event){
            var elem = document.querySelector('.items');
            $scope.iso = new Isotope(elem, {
                // options
                itemSelector: '.item',
                percentPosition: true,
                masonry: {
                    // use element for option
                    columnWidth: '.item-sizer'
                }
            });
        });
    });
})();