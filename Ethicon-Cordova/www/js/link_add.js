(function(){
    var module = angular.module('app');

    module.controller('LinkAddController', function($scope, $data, $http, apiService, networkService, databaseService) {
        $scope.link_add_done = function() {
            
            var url = $data.apiAddress + '/createlink/';
            console.log('create link url: ' + url);
            
            var postData = {};
            postData.name = $scope.addedLinkName;
            postData.url = $scope.addedLinkUrl;
            
            if (postData.name == null) {
                alert("Please input name.");
                return;
            }
            if (postData.url == null) {
                alert("Please input url.");
                return;
            }

            if (networkService.isNetworkAvailable() == true) {
                ActivityIndicator.show('Please wait');
                
                $http.post(url, postData,
                {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    transformRequest: apiService.transform
                }).success(function(data, status, headers, config) {
                    alert('New EIFU is created successfully.');
                    console.log("create link response: " + JSON.stringify(data));
                    databaseService.clearDB();
                    apiService.getAllData();
                    $scope.navi.popPage();
                })
                .error(function(data, status, headers, config) {
                    alert("Error: " + JSON.stringify(data));
                });
            } else {
                alert('Network is not available.\nPlease check your network status');
            }
        }
    });
})();