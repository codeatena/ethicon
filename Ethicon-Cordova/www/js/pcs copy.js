(function(){
    
    var module = angular.module('app');
    
    module.controller('PCsController', function($scope, $data) {
        
        $scope.arrPCs = $data.arrPCs;
        $scope.rootPath = $data.rootPath;
        $scope.serverAddress = $data.serverAddress;
        
        $scope.go_detail = function(index) {
            $data.pc = $data.arrPCs[index];
            $scope.navi.pushPage('pc_detail.html');
        }
        
        $scope.go_add = function() {
            $scope.navi.pushPage('pc_add.html');
        }
    });
})();