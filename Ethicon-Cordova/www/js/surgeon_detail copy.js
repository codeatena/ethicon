(function(){
    
    var module = angular.module('app');
    
    module.controller('SurgeonDetailController', function($scope, $data) {
        $scope.surgeon = $data.surgeon;
    });
})();