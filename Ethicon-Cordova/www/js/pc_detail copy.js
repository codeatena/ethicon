(function(){
    'use strict';
    var module = angular.module('app');
    
    module.controller('PCDetailController', function($scope, $data) {
        
        $scope.pc = $data.pc;
        $scope.serverAddress = $data.serverAddress;
        $scope.rootPath = $data.rootPath;
        
        $scope.go_surgeon_detail = function(surgeon_id) {
            for (var i = 0; i < $data.arrSurgeons.length; i++) {
                if (surgeon_id == $data.arrSurgeons[i].id) {
                    $data.surgeon = $data.arrSurgeons[i];
                }
            }
            $scope.navi.pushPage("surgeon_detail.html");
        }
    });
})();