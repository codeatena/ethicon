angular.module('app').factory('productService',
    function($data, surgeonService) {
        
        var factory = {};
        var tableName = 'product';
        
        factory.readAllRecords = function() {
            var arrRecords = [] ;
            console.log(tableName + ' table getRecords');
            var db = window.openDatabase("ethicon", "1.0", "Ethicon", 200000);
            db.transaction(populateDB, errorCB, successCB);
            
            function successCB() {
                console.log(tableName + " table read success.");
                
                for (var i = 0; i < arrRecords.length; i++) {
                    arrRecords[i].category_name = getCategoryName(arrRecords[i].category_id);
                }
                
                $data.arrProducts = arrRecords;
                surgeonService.readAllRecords();
                
                function getCategoryName(category_id) {
                    if (category_id == 1) {
                        return "Endo Mechanical";
                    }
                    if (category_id == 2) {
                        return "Advanced Energy";
                    }
                    if (category_id == 3) {
                        return "Wound Closure";
                    }
                    if (category_id == 4) {
                        return "Biosurgery";
                    }
                }
            }
            
            function errorCB(err) {
                console.log(tableName + " Error processing SQL: "+err.code);
                alert("Error processing SQL: "+err.code);
            }
            
            function populateDB(tx) {
                console.log(tableName + " populateDB");
                tx.executeSql('SELECT * FROM ' + tableName, [], querySuccess, errorCB);
            }
            
            function querySuccess(tx, results) {
                var len = results.rows.length;
                console.log(tableName + " table: " + len + " rows found.");
                for (var i = 0; i < len; i++) {
                    arrRecords[i] = results.rows.item(i);
                }
            }
        }
        
        return factory;
    }
);