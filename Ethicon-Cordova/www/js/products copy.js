(function() {

    var module = angular.module('app');

    module.controller('ProductsController', function($scope, $data) {

        $scope.arrProducts = [];

        for (var i = 0; i < $data.arrProducts.length; i++) {
            if ($data.category_id == $data.arrProducts[i].category_id) {
                $scope.arrProducts.push($data.arrProducts[i]);
            }
        }

        $scope.go_detail = function(index) {
            $data.product = $data.arrProducts[index];
            $scope.navi.pushPage('product_detail.html');
        }

        $scope.go_add = function() {
            $scope.navi.pushPage('product_add.html');
        }
    });
})();