<?php

class JsonResponse{
    public $status;
    public $data;
    public $code;
    public $message;

    function decode($status, $data, $code){
        if($status!="error"){
            $this->data=$data;
        }
        else{
            $this->message=$data;
        }
        $this->status=$status;
        $this->code=$code;
        return CJSON::encode($this);
    }

}