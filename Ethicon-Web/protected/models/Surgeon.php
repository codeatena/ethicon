<?php

/**
 * This is the model class for table "surgeon".
 *
 * The followings are the available columns in table 'surgeon':
 * @property string $id
 * @property string $name
 * @property string $specialty
 * @property string $contact_number
 * @property string $hospital_id
 *
 * The followings are the available model relations:
 * @property PreferenceCard[] $preferenceCards
 * @property Hospital $hospital
 */
class Surgeon extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'surgeon';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hospital_id', 'required'),
			array('name, specialty, contact_number', 'length', 'max'=>100),
			array('hospital_id', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, specialty, contact_number, hospital_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'preferenceCards' => array(self::HAS_MANY, 'PreferenceCard', 'surgeon_id'),
			'hospital' => array(self::BELONGS_TO, 'Hospital', 'hospital_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'specialty' => 'Specialty',
			'contact_number' => 'Contact Number',
			'hospital_id' => 'Hospital',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('specialty',$this->specialty,true);
		$criteria->compare('contact_number',$this->contact_number,true);
		$criteria->compare('hospital_id',$this->hospital_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Surgeon the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
