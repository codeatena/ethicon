<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Ethicon',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.models.exceptions.*',
		'application.components.*',
		'application.extensions.*',
		'application.utils.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'XCVA123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),

	),

	// application components
	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
            //'showScriptName'=>false,
            'caseSensitive'=>false,
            'rules'=>array(
                // REST patterns
                array('api/get', 'pattern'=>'api/<model:\w+>/<id:\d+>/<token>', 'verb'=>'GET'),
                array('api/get', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'GET'),

                //array('api/update', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'POST'),
                //array('api/addcoins', 'pattern'=>'api/<model:\w+>/<id:\d+>/addcoins', 'verb'=>'PUT'),
                //array('api/subtractcoins', 'pattern'=>'api/<model:\w+>/<id:\d+>/subtractcoins', 'verb'=>'PUT'),
                array('api/<action>', 'pattern'=>'api/<action:\w+>', 'verb'=>'POST'),
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                //'<action:\w+>'=>'site/<action>',
			),
		),


		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning,info',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

        'clientScript'=>array(
            'packages'=>array(
                'jquery'=>array(                             // set the new jquery
                    'baseUrl'=>'https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/',
                    'js'=>array('jquery.min.js'),
                ),
                'bootstrap'=>array(                       //set others js libraries
                    'baseUrl'=>'//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/',
                    'js'=>array('js/bootstrap.min.js'),
                    'css'=>array(                        // and css
                        'css/bootstrap.min.css',
                    ),
                    'depends'=>array('jquery'),         // cause load jquery before load this.
                ),
            ),
        ),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'info@elitemarketing.io',
	),
);
