-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2015 at 11:38 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ethicon`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'category1'),
(2, 'category2'),
(3, 'category3'),
(4, 'category4');

-- --------------------------------------------------------

--
-- Table structure for table `hospital`
--

CREATE TABLE IF NOT EXISTS `hospital` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospital`
--

INSERT INTO `hospital` (`id`, `name`) VALUES
(1, 'hospital1'),
(2, 'hospital2');

-- --------------------------------------------------------

--
-- Table structure for table `link`
--

CREATE TABLE IF NOT EXISTS `link` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `url` varchar(400) DEFAULT NULL,
  `image_url` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `link`
--

INSERT INTO `link` (`id`, `name`, `url`, `image_url`) VALUES
(1, 'jhgbh', 'http://www.w3schools.com/html/default.asp', 'img/links/AX55G.png'),
(2, 'fhbrtb', 'http://www.w3schools.com/css/default.asp', 'img/links/TCR10.png'),
(31, 'gfda', 'http://www.w3schools.com/js/default.asp', 'img/links/TL90.png'),
(32, 'dsagbnjtj', 'http://www.w3schools.com/sql/default.asp', 'img/links/TRH90.png'),
(33, 'riujqj', 'http://www.w3schools.com/jquery/default.asp', 'img/links/TRT75.png');

-- --------------------------------------------------------

--
-- Table structure for table `preference_card`
--

CREATE TABLE IF NOT EXISTS `preference_card` (
  `id` bigint(20) NOT NULL,
  `card_url` varchar(100) DEFAULT NULL,
  `surgeon_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `preference_card`
--

INSERT INTO `preference_card` (`id`, `card_url`, `surgeon_id`) VALUES
(1, 'img/preference_cards/1.png', 3),
(2, 'img/preference_cards/2.jpg', 3),
(3, 'img/preference_cards/3.jpg', 4),
(4, 'img/preference_cards/e4d2658ef31cf35bb7a1dd26ddedeed7.jpg', 4),
(5, 'img/preference_cards/0639e41e83e7b86e65d7e25b1b030ef0.jpg', 3),
(6, 'img/preference_cards/cf18af6a12bd42925507b2ab95030621.jpg', 7);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `pdf_url` varchar(400) DEFAULT NULL,
  `video_url` varchar(400) DEFAULT NULL,
  `image_url` varchar(255) NOT NULL,
  `category_id` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `pdf_url`, `video_url`, `image_url`, `category_id`) VALUES
(1, 'product1', 'pdf/products/product1.pdf', 'video/products/video1.mp4', 'img/products/circular_staplers/SDH29A.png', 2),
(2, 'product2', 'pdf/products/product2.pdf', 'video/products/video2.mp4', 'img/products/curved_staplers/CS40G.png', 1),
(3, 'product3', 'pdf/products/product3.pdf', 'video/products/video3.mp4', 'img/products/endo_cutters/TR35B.png', 3),
(4, 'product4', 'pdf/products/product4.pdf', 'video/products/video4.mp4', 'img/products/linear_staplers_cutters/TRT75.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `surgeon`
--

CREATE TABLE IF NOT EXISTS `surgeon` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `specialty` varchar(100) DEFAULT NULL,
  `contact_number` varchar(100) DEFAULT NULL,
  `hospital_id` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surgeon`
--

INSERT INTO `surgeon` (`id`, `name`, `specialty`, `contact_number`, `hospital_id`) VALUES
(3, 'surgeon_name1', 'specialty1', 'contact_number1', 1),
(4, 'surgeon_name2', 'specialty2', 'contact_number2', 1),
(7, 'surgeon_name3', 'specialty3', 'contact_number3', 1),
(10, 'surgeon_name4', 'specialty4', 'contact_number4', 1),
(11, 'surgeon_name5', 'specialty5', 'contact_number5', 1),
(12, 'surgeon_name6', 'specialty6', 'contact_number6', 1),
(13, 'surgeon_name7', 'specialty7', 'contact_number7', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hospital`
--
ALTER TABLE `hospital`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `link`
--
ALTER TABLE `link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preference_card`
--
ALTER TABLE `preference_card`
  ADD PRIMARY KEY (`id`), ADD KEY `preference_card_surgeon_FK` (`surgeon_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`), ADD KEY `NewTable_category_FK` (`category_id`);

--
-- Indexes for table `surgeon`
--
ALTER TABLE `surgeon`
  ADD PRIMARY KEY (`id`), ADD KEY `surgeon_hospital_FK` (`hospital_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hospital`
--
ALTER TABLE `hospital`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `link`
--
ALTER TABLE `link`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `preference_card`
--
ALTER TABLE `preference_card`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `surgeon`
--
ALTER TABLE `surgeon`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `preference_card`
--
ALTER TABLE `preference_card`
ADD CONSTRAINT `preference_card_surgeon_FK` FOREIGN KEY (`surgeon_id`) REFERENCES `surgeon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
ADD CONSTRAINT `NewTable_category_FK` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `surgeon`
--
ALTER TABLE `surgeon`
ADD CONSTRAINT `surgeon_hospital_FK` FOREIGN KEY (`hospital_id`) REFERENCES `hospital` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
