<?php

class ApiController extends CController
{
    // Members
    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';

    // Actions
    public function actionGet()
    {
        // Check if id was submitted via GET
        if (!isset($_GET['id']))
            $this->_sendResponse(400, 'Error: Parameter <b>id</b> is missing');

        switch ($_GET['model']) {
            // Find respective model
            case 'surgeon':
                $model = Surgeon::model()->findByPk($_GET['id']);
                if ($model == null) {
                    $model = new Surgeon();
                    $model->id = $_GET['id'];
                    $model->save();
                }
                break;
            default:
                $this->_sendResponse(501, sprintf('Mode <b>get</b> is not implemented for model <b>%s</b>', $_GET['model']));
                Yii::app()->end();
        }
        // Did we find the requested model? If not, raise an error
        if (is_null($model))
            $this->_sendResponse(404, 'No Item found with id ' . $_GET['id']);
        else
            $this->_sendResponse(200, $model);
    }

    public function actionGetSurgeons()
    {
        if(isset($_GET['from']) && isset($_GET['limit'])) {
            $from = $_GET['from'];
            $limit = $_GET['limit'];
            $findCriteria = new CDbCriteria();
            $findCriteria->offset = $from;
            $findCriteria->limit = $limit;
            $models = Surgeon::model()->findAll($findCriteria);
        }
        else{
            $models = Surgeon::model()->findAll();
        }
        $this->_sendResponse(200, $models);
    }

    public function actionGetAllData()
    {
        $models = array();
        if(isset($_GET['hospital_id'])) {
            $hospital_id = $_GET['hospital_id']; //this should have some security to avoid any person to access to the hospital information
            $hospital = Hospital::model()->findByPk($hospital_id);
            if ($hospital != null) {
                $models['hospital']['id'] = $hospital->id;
                $i=0;
                foreach($hospital->surgeons as $surgeon){
                    $models['hospital']['surgeons'][$i]=array();
                    $models['hospital']['surgeons'][$i][]=$surgeon;
                    $models['hospital']['surgeons'][$i][]['preference_cards']=$surgeon->preferenceCards;
                    $i++;
                }
            }
        }
        $models['products']=Product::model()->findAll();
        $models['links']=Link::model()->findAll();
        $this->_sendResponse(200, $models);
    }

    public function actionCreatePreferenceCard()
    {
        $model = new PreferenceCard();
        // Try to assign POST values to attributes
        $model->surgeon_id=$_POST['surgeon_id'];

        if(!$model->validate(array("surgeon_id"))){
            $this->_sendResponse(500, $model->errors['surgeon_id']);
        }
        Yii::log("The surgeon id: $model->surgeon_id");

        $surgeon=Surgeon::model()->findByPk($model->surgeon_id);
        if($surgeon==null){
            $this->_sendResponse(500, "Surgeon with id $model->surgeon_id does not exist");
        }
        $model->surgeon=$surgeon;

        if (isset($_FILES['file'])) {
            $image = CUploadedFile::getInstanceByName('file');
            $ext = strstr($image->name, '.');
            $new_file_name  = md5($image->name . mktime() . $model->surgeon_id) . $ext;
            Yii::log("This is the new file name: $new_file_name  ","info");
            $model->card_url = $new_file_name;
            $image->saveAs("img/uploads/$new_file_name");

            // Try to save the model
            if ($model->save()) {
                $this->_sendResponse(200, CJSON::encode($model));
            } else {
                $this->manageErrors($model,"Preference card");
            }
        }
        else{
            $this->_sendResponse(500, "File no uploaded");
        }
    }
	
    public function actionCreateLink()
    {
        $model = new Link();
        // Try to assign POST values to attributes
		
        foreach($_POST as $var=>$value) {
            // Does the model have this attribute? If not raise an error
            if($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500,sprintf('Parameter <b>%s</b> is not allowed for Link', $var));
        }
		
        // Try to save the model
        if($model->save())
            $this->_sendResponse(200, CJSON::encode($model));
        else {
            $this->manageErrors($model,"Link");
        }
    }
	
	public function actionCreateSurgeon() {
		$model = new Surgeon();
		// Try to assign POST values to attributes

		foreach($_POST as $var=>$value) {
            // Does the model have this attribute? If not raise an error
            if($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500,sprintf('Parameter <b>%s</b> is not allowed for Link', $var));
        }
		
		// Try to save the model
        if($model->save())
            $this->_sendResponse(200, CJSON::encode($model));
        else {
            $this->manageErrors($model,"Surgeon");
        }
	}
	
	public function actionCreateProduct() {
		$model = new Product();
		// Try to assign POST values to attributes

		foreach($_POST as $var=>$value) {
            // Does the model have this attribute? If not raise an error
            if($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500,sprintf('Parameter <b>%s</b> is not allowed for Link', $var));
        }
		
		// Try to save the model
        if($model->save())
            $this->_sendResponse(200, CJSON::encode($model));
        else {
            $this->manageErrors($model,"Product");
        }
	}

    private function manageErrors($model, $name){
        // Errors occurred
        $msg = "<h1>Error</h1>";
        $msg .= sprintf("Couldn't create model <b>%s</b>", $name);
        $msg .= "<ul>";
        foreach($model->errors as $attribute=>$attr_errors) {
            $msg .= "<li>Attribute: $attribute</li>";
            $msg .= "<ul>";
            foreach($attr_errors as $attr_error)
                $msg .= "<li>$attr_error</li>";
            $msg .= "</ul>";
        }
        $msg .= "</ul>";
        $this->_sendResponse(500, $msg );
    }

    private function _sendResponse($status = 200, $body = '', $content_type = 'application/json')
    {
        // set the status
        //$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        //header($status_header);
        // and the content type
        $this->_http_response_code($status); //http_response_code is from php 5.4
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if ($status == 200) {
            // send the body
            $response = new JsonResponse();
            echo $response->decode("success", $body, $status);
        } // we need to create the body if none is passed
        elseif ($body != '') {
            $response = new JsonResponse();
            echo $response->decode("error", $body, $status);
        } else {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch ($status) {
                case 400:
                    $message = 'Bad request.';
                    break;
                case 401:
                    $message = 'You must be logged in at Instagram.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            $response = new JsonResponse();
            echo $response->decode("error", $message, $status);

        }
        Yii::app()->end();
    }


    private function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    private function _saveModel($model, $errorMessage)
    {
        // Try to save the model
        if ($model->save())
            $this->_sendResponse(200, $model);
        else {
            $this->printErrorWithModel($model, $errorMessage);
        }
    }

    /**
     * @param $model
     * @param $errorMessage
     */
    private function printErrorWithModel($model, $errorMessage)
    {
        // Errors occurred
        $msg = $errorMessage . " ";
        foreach ($model->errors as $attribute => $attr_errors) {
            $msg .= $attribute . ": ";
            foreach ($attr_errors as $attr_error)
                $msg .= $attr_error . " ";
        }
        $this->_sendResponse(500, $msg);
    }

    private function _http_response_code($newCode = NULL)
    {
        static $code = 200;
        if ($newCode !== NULL) {
            header('X-PHP-Response-Code: ' . $newCode, true, $newCode);
            if (!headers_sent())
                $code = $newCode;
        }
        return $code;

    }
}
