<?php
/**
 * Created by PhpStorm.
 * User: Mauro
 * Date: 3/30/15
 * Time: 2:15 PM
 */

class Igers {

    static function sendOrder($link,$amount,$print=false){
        $Reseller = new Igerslike();
        /**
         * ORDER ADD
         * Syntax: $RESELLER->ORDER(LINK,TYPE,AMOUNT);
         */

        if($print){
            echo "Link: $link Amount: $amount <br>\n\n";
        }

        $Response = $Reseller->order($link,'ig_likes',$amount);
        if($Response == null)
        {
            if($print){
                echo "Cannot Connect to IGERSLIKE! Probably Web-Site or Down or API URL not Reachable<br>\n\n";
            }
            return false;
            // All Went good ,but no response from our server, please try again later
        }
        if($Response->status == 'ok') // Status OK ! Necessary Fields Passed
        {
            if($Response->order != '')
            {
                $OrderID = $Response->order; // This is the Order ID from IGERSLIKE! Use it or Save it to Database to check the status Later!
                if($print){
                echo "Your Order was Inserted! The ID is : $OrderID";
                echo "<br>\n\n";
                }
                return true;
            }
        }
            else
            {
                $errorMessage = $Response->message;
                throw new IgersLikeException("Failed to insert order! Error : $errorMessage");
            }
        return false;

        /**
         * ORDER STATUS
         * Syntax: $RESELLER->STATUS(ID);
         */

       /* $ResponseStatus = $Reseller->status($OrderID);
        if($ResponseStatus->status == "ok")
        {
            $Status = $ResponseStatus->order_status;
            echo "Order Status is : $Status";
            echo "<br>";
            // To see the full ouput uncomment the lines
            // var_dump($ResponseStatus);
            // echo "<br>";

        }
        else
        {
            // Failed!
            $ErrorMessage = $ResponseStatus->message;
            echo "Failed to Search Order ! Error : $ErrorMessage";
            echo "<br>";
        }*/
    }

} 