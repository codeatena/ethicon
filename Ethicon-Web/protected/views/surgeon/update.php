<?php
/* @var $this SurgeonController */
/* @var $model Surgeon */

$this->breadcrumbs=array(
	'Surgeons'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Surgeon', 'url'=>array('index')),
	array('label'=>'Create Surgeon', 'url'=>array('create')),
	array('label'=>'View Surgeon', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Surgeon', 'url'=>array('admin')),
);
?>

<h1>Update Surgeon <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>