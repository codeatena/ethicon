<?php
/* @var $this SurgeonController */
/* @var $model Surgeon */

$this->breadcrumbs=array(
	'Surgeons'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Surgeon', 'url'=>array('index')),
	array('label'=>'Create Surgeon', 'url'=>array('create')),
	array('label'=>'Update Surgeon', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Surgeon', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Surgeon', 'url'=>array('admin')),
);
?>

<h1>View Surgeon #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'specialty',
		'contact_number',
	),
)); ?>
