<?php
/* @var $this SurgeonController */
/* @var $model Surgeon */
/* @var $form CActiveForm */
?>

<div class="form container">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'surgeon-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form'),
)); ?>

    <?php
    $labelClasses = 'col-xs-12 col-sm-4 col-md-4 control-label';
    $inputDivClasses = 'col-xs-12 col-sm-4 col-md-4';
    $errorClasses = 'col-xs-12 col-sm-4 col-md-4 control-label errorOnInput';
    ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="form-group <?php echo $model->hasErrors('name')?'has-error':''?>">
        <?php echo $form->labelEx($model,'name', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>100,'class' => 'form-control')); ?>
        </div>
        <?php echo $form->error($model,'name', array('class' => $errorClasses)); ?>
    </div>


    <div class="form-group <?php echo $model->hasErrors('specialty')?'has-error':''?>">
        <?php echo $form->labelEx($model,'specialty', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model,'specialty',array('size'=>50,'maxlength'=>100,'class' => 'form-control')); ?>
        </div>
        <?php echo $form->error($model,'specialty', array('class' => $errorClasses)); ?>
    </div>

    <div class="form-group <?php echo $model->hasErrors('contact_number')?'has-error':''?>">
        <?php echo $form->labelEx($model,'contact_number', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model,'contact_number',array('size'=>50,'maxlength'=>100,'class' => 'form-control')); ?>
        </div>
        <?php echo $form->error($model,'contact_number', array('class' => $errorClasses)); ?>
    </div>

    <div class="text-center">
        <button class="btn btn-primary" onclick="submit()"><?php echo $model->isNewRecord ? 'Create' : 'Save'?></button>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->