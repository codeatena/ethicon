<?php
/* @var $this SurgeonController */
/* @var $model Surgeon */
/* @var $form CActiveForm */
?>

<div class="wide form container">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form'),
)); ?>

    <?php
    $labelClasses = 'col-xs-12 col-sm-4 col-md-4 control-label';
    $inputDivClasses = 'col-xs-12 col-sm-4 col-md-4';
    ?>

    <div class="form-group">
        <?php echo $form->label($model,'id', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model,'id', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->label($model,'name', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>100,'class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->label($model,'specialty', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model,'specialty', array('size'=>50,'maxlength'=>100,'class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->label($model,'contact_number', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model,'contact_number',array('size'=>50,'maxlength'=>100,'class' => 'form-control')); ?>
        </div>
    </div>

    <div class="text-center">
        <button class="btn btn-primary" onclick="submit()">Search</button>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->