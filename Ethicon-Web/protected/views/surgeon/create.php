<?php
/* @var $this SurgeonController */
/* @var $model Surgeon */

$this->breadcrumbs=array(
	'Surgeons'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Surgeon', 'url'=>array('index')),
	array('label'=>'Manage Surgeon', 'url'=>array('admin')),
);
?>

<h1>Create Surgeon</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>