<?php
/* @var $this SurgeonController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Surgeons',
);

$this->menu=array(
	array('label'=>'Create Surgeon', 'url'=>array('create')),
	array('label'=>'Manage Surgeon', 'url'=>array('admin')),
);
?>

<h1>Surgeons</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
