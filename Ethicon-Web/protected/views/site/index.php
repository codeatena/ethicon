<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<div class="list-group">
    <?php echo CHtml::link('Hospitals',array('hospital/'),array('class'=>'list-group-item')); ?>
    <?php echo CHtml::link('Surgeons',array('surgeon/'),array('class'=>'list-group-item')); ?>
    <?php echo CHtml::link('Preference Card',array('preferencecard/'),array('class'=>'list-group-item')); ?>
</div>