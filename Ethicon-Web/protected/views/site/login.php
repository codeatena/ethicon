<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h1>Login</h1>

<p>Please fill out the following form with your login credentials:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
    'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form'),
)); ?>

    <?php
    $labelClasses = 'col-xs-12 col-sm-4 col-md-4 control-label';
    $inputDivClasses = 'col-xs-12 col-sm-4 col-md-4';
    $errorClasses = 'col-xs-12 col-sm-4 col-md-4 estadisticaError control-label';
    ?>

    <div class="form-group <?php echo $model->hasErrors('username')?'has-error':''?>">
        <?php echo $form->label($model, 'username', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
        </div>
        <?php echo $form->error($model, 'username', array('class' => $errorClasses)); ?>
    </div>

    <div class="form-group <?php echo $model->hasErrors('password')?'has-error':''?>">
        <?php echo $form->label($model, 'password', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
        </div>
        <?php echo $form->error($model, 'password', array('class' => $errorClasses)); ?>
    </div>

    <div class="form-group">
        <div class="<?php echo $inputDivClasses?> col-md-offset-4 col-sm-offset-4">
            <?php echo $form->checkBox($model, 'rememberMe'); ?>
            <?php echo $form->label($model, 'rememberMe'); ?>
        </div>
        <?php echo $form->error($model, 'rememberMe'); ?>
    </div>

    <div class="text-center">
        <button class="btn btn-primary" onclick="submit()">Login</button>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
