<?php
/* @var $this PreferenceCardController */
/* @var $model PreferenceCard */
/* @var $form CActiveForm */
?>

<div class="wide form container">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form'),
)); ?>

    <?php
    $labelClasses = 'col-xs-12 col-sm-4 col-md-4 control-label';
    $inputDivClasses = 'col-xs-12 col-sm-4 col-md-4';
    ?>

    <div class="form-group">
        <?php echo $form->label($model,'id', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model,'id', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->label($model,'card_url', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model,'card_url', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->label($model,'surgeon_id', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model,'surgeon_id', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="text-center">
        <button class="btn btn-primary" onclick="submit()">Search</button>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->