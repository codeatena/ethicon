<?php
/* @var $this PreferenceCardController */
/* @var $model PreferenceCard */

$this->breadcrumbs=array(
	'Preference Cards'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PreferenceCard', 'url'=>array('index')),
	array('label'=>'Create PreferenceCard', 'url'=>array('create')),
	array('label'=>'View PreferenceCard', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PreferenceCard', 'url'=>array('admin')),
);
?>

<h1>Update PreferenceCard <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>