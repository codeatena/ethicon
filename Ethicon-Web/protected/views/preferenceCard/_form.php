<?php
/* @var $this PreferenceCardController */
/* @var $model PreferenceCard */
/* @var $form CActiveForm */
?>

<div class="form container">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'preference-card-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form'),
)); ?>

    <?php
    $labelClasses = 'col-xs-12 col-sm-4 col-md-4 control-label';
    $inputDivClasses = 'col-xs-12 col-sm-4 col-md-4';
    $errorClasses = 'col-xs-12 col-sm-4 col-md-4 control-label errorOnInput';
    ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="form-group <?php echo $model->hasErrors('card_url')?'has-error':''?>">
        <?php echo $form->labelEx($model,'card_url', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model,'card_url',array('size'=>100,'maxlength'=>100,'class' => 'form-control')); ?>
        </div>
        <?php echo $form->error($model,'card_url', array('class' => $errorClasses)); ?>
    </div>

    <div class="form-group <?php echo $model->hasErrors('surgeon_id')?'has-error':''?>">
        <?php echo $form->labelEx($model,'surgeon_id', array('class' => $labelClasses)); ?>
        <div class="<?php echo $inputDivClasses ?>">
            <?php echo $form->textField($model,'surgeon_id',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
        </div>
        <?php echo $form->error($model,'surgeon_id', array('class' => $errorClasses)); ?>
    </div>

    <div class="text-center">
        <button class="btn btn-primary" onclick="submit()"><?php echo $model->isNewRecord ? 'Create' : 'Save'?></button>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->