<?php
/* @var $this PreferenceCardController */
/* @var $data PreferenceCard */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('card_url')); ?>:</b>
	<?php echo CHtml::encode($data->card_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('surgeon_id')); ?>:</b>
	<?php echo CHtml::encode($data->surgeon_id); ?>
	<br />


</div>