<?php
/* @var $this PreferenceCardController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Preference Cards',
);

$this->menu=array(
	array('label'=>'Create PreferenceCard', 'url'=>array('create')),
	array('label'=>'Manage PreferenceCard', 'url'=>array('admin')),
);
?>

<h1>Preference Cards</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
