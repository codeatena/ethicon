<?php
/* @var $this PreferenceCardController */
/* @var $model PreferenceCard */

$this->breadcrumbs=array(
	'Preference Cards'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PreferenceCard', 'url'=>array('index')),
	array('label'=>'Manage PreferenceCard', 'url'=>array('admin')),
);
?>

<h1>Create PreferenceCard</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>