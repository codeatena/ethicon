<?php
/* @var $this PreferenceCardController */
/* @var $model PreferenceCard */

$this->breadcrumbs=array(
	'Preference Cards'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PreferenceCard', 'url'=>array('index')),
	array('label'=>'Create PreferenceCard', 'url'=>array('create')),
	array('label'=>'Update PreferenceCard', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PreferenceCard', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PreferenceCard', 'url'=>array('admin')),
);
?>

<h1>View PreferenceCard #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'card_url',
		'surgeon_id',
	),
)); ?>
