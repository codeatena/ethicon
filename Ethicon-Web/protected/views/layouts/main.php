<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo Yii::app()->request->baseUrl; ?>"><?php echo CHtml::encode(Yii::app()->name); ?></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hospitals <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><?php echo CHtml::link('Create',array('hospital/create')); ?></li>
                        <li><?php echo CHtml::link('Manage',array('hospital/admin')); ?></li>
                        <li><?php echo CHtml::link('List',array('hospital/')); ?></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Surgeons <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><?php echo CHtml::link('Create',array('surgeon/create')); ?></li>
                        <li><?php echo CHtml::link('Manage',array('surgeon/admin')); ?></li>
                        <li><?php echo CHtml::link('List',array('surgeon/')); ?></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Preference cards <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><?php echo CHtml::link('Create',array('preferencecard/create')); ?></li>
                        <li><?php echo CHtml::link('Manage',array('preferencecard/admin')); ?></li>
                        <li><?php echo CHtml::link('List',array('preferencecard/')); ?></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if(Yii::app()->user->isGuest){?>
                    <li><?php echo CHtml::link('Login',array('site/login')); ?></li>
                <?php }else{?>
                    <li><?php echo CHtml::link('Logout ('.Yii::app()->user->name.')',array('site/logout')); ?></li>
                <?php }?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container" id="page">

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?><br/>
		All Rights Reserved.<br/>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
