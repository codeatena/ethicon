<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main');
$firstColumn="";
$secondColumn="";
if (count($this->menu)>0){
    $firstColumn="col-xs-12 col-sm-8 col-md-8";
    $secondColumn="col-xs-12 col-sm-2 col-md-2";
}
?>

<div class="<?php echo $firstColumn; ?>">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="<?php echo $secondColumn; ?>">
	<div id="sidebar">
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
	?>
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>